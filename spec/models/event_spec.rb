require 'rails_helper'

# Test suite for the Event model
RSpec.describe Event, type: :model do
  # Association test
  # ensure an item record belongs to a single event record
  it { should belong_to(:user) }
  # Validation test
  it { should validate_presence_of(:status) }
  it { should validate_numericality_of(:duration).only_integer.allow_nil }
end
