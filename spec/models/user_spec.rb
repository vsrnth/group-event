require 'rails_helper'

# Test suite for the USer model
RSpec.describe User, type: :model do
  # Association test
  # ensure User model has a 1:m relationship with the Event model
  it { should have_many(:events) }
  # Validation tests
  # ensure column name is present before saving
  it { should validate_presence_of(:name) }
end
