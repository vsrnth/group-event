FactoryBot.define do
  factory :event do
    name {Faker::Company.name}
    description {Faker::Company.catch_phrase}
    location {Faker::Nation.capital_city}
    user_id {Faker::Number.between(1, 1)}
    status {"Draft"}
  end
  factory :published_event do
    start_date { Faker::Date.between_except(2.month.ago, 1.year.from_now, Date.today).strftime("%d-%m-%Y") }
    end_date { Faker::Date.between_except(1.month.ago, 1.year.from_now, Date.today).strftime("%d-%m-%Y") }
    duration {(end_date - start_date).to_i}
    name { Faker::Company.name }
    description {Faker::Company.catch_phrase}
    location {Faker::Nation.capital_city}

  end
end
