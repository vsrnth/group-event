# frozen_string_literal: true

require 'rails_helper'

describe 'Event Deletion' do
  before(:all) do
    base_url = ENV['server_url'] + '/events'
    event_object = RestClient.post base_url, duration: 30, start_date: '2018-11-13', end_date: '2018-12-13', name: 'Random Event', user_id: '1'
    @event_id = JSON.parse(event_object.body)['id']
    url = "#{base_url}/#{@event_id}"
    RestClient.delete url
    @response = RestClient.get url
    @response_body = JSON.parse(@response.body)
  end

  it 'should change status to deleted when event is destroyed' do
    expect(@response_body['status']).to eq('Deleted')
  end
  it 'should match the expected response event id' do
    expect(@response_body['id']).to eq(@event_id)
  end
  it 'should be able to fetch event if the event id exists' do
    expect(@response.code).to eq(200)
  end
end
