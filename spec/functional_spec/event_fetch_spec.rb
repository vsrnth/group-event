# frozen_string_literal: true

require 'rails_helper'

describe 'Event Fetching' do
  before(:all) do
    base_url = ENV['server_url'] + '/events'
    event_id = 10
    url = "#{base_url}/#{event_id}"
    @response = RestClient.get url
    @response_body = JSON.parse(@response.body)
  end

  it 'should be able to fetch event if the event id exists' do
    expect(@response.code).to eq(200)
  end
  it 'should match the expected response id ' do
    expect(@response_body['id']).to eq(10)
  end

  it 'should match the expected response duration ' do
    expect(@response_body['duration']).to eq(30)
  end
  it 'should match the expected response start_date ' do
    expect(@response_body['start_date']).to eq('2018-11-13')
  end
  it 'should match the expected response end_date ' do
    expect(@response_body['end_date']).to eq('2018-12-13')
  end
  it 'should match the expected response name ' do
    expect(@response_body['name']).to eq('"random_event"')
  end
  it 'should match the expected response description ' do
    expect(@response_body['description']).to eq('"Event Details"')
  end
  it 'should match the expected response location ' do
    expect(@response_body['location']).to eq('bangalore')
  end
  it 'should match the expected response user_id ' do
    expect(@response_body['user_id']).to eq(1)
  end
end
