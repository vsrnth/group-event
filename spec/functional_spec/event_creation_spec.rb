# frozen_string_literal: true

require 'rails_helper'

describe 'Event Creation' do
  before(:all) do
    base_url = ENV['server_url'] + '/events'

    data = { user_id: 1, location: 'bangalore' }
    @response = RestClient.post base_url, data
    @response_body = JSON.parse(@response.body)
  end

  it 'should return 201 CREATED on sucessful creation' do
    expect(@response.code).to eq(201)
  end

  it 'should have status as draft when any field is missing' do
    expect(@response_body['status']).to eq('Draft')
  end

  it 'should have non-specifield as null' do
    expect(@response_body['duration']).to eq(nil)
    expect(@response_body['start_date']).to eq(nil)
    expect(@response_body['end_date']).to eq(nil)
    expect(@response_body['name']).to eq(nil)
    expect(@response_body['description']).to eq(nil)
  end

  it 'should match the expected response user_id ' do
    expect(@response_body['user_id']).to eq(1)
  end
  context 'should return 400 bad request' do
    it 'should return 400 Bad request when user_id not specified' do
      url = ENV['server_url'] + '/events?location=bangalore'
      begin
        RestClient::Request.execute(method: :post, url: url)
      rescue RestClient::ExceptionWithResponse => err
      end
      expect(err.response.code).to be(400)
    end
  end

  context 'should set status as published when required fields are presennt' do
    before(:all) do
      base_url = ENV['server_url'] + '/events'
      data = { duration: 30, start_date: '2018-11-13', end_date: '2018-12-13', name: 'Random Event Name', description: 'Event Description', user_id: 1, location: 'bangalore' }
      @response = RestClient.post base_url, data
      @response_body = JSON.parse(@response.body)
    end
    it 'should set status as published when required fields are present' do
      expect(@response_body['status']).to eq('Published')
    end
  end
end
