# README

##### Ruby and Rails versions
 - Ruby version: ruby 2.5.1p57 (2018-03-29 revision 63029) [x86_64-linux]
 - Rails version: Rails 5.2.1

##### Configuration
`bundle install` - to install all the gems
##### Database initialization
- `rails db:migrate`
- `rails db:seed RAILS_ENV=development`
##### How to run the test suite
- Run `rails db:seed RAILS_ENV=test` before running the test suite
- Functional specs are under spec/functional_spec
- Request specs are under spec/request
- Model specs are under spec/models
- `bundle exec rspec` - This will run the whole test suite
- `bundle exec rspec/functional_spec/*.rb` - To run the functional specs
  - Run the `rails s` before running the functional specs
- `bundle exec rspec/request/*.rb` - To run the Request specs

