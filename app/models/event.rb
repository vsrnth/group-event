class Event < ApplicationRecord
  belongs_to :user
  validates_numericality_of :duration, only_integer: true, allow_nil: true
  validates_presence_of :status
  ALL_COLUMNS = %w[duration start_date end_date name description location user_id].freeze

  def self.define_status(event_params)
    event_params = event_params.to_h
    event_params['status'] = event_params.keys == ALL_COLUMNS ? 'Published' : 'Draft'
    event_params
  end

  def self.time_and_duration_setup(event_params)
    event_params = event_params.to_h
    event_params = define_duration(event_params) if event_params['duration'].nil?
    event_params = define_start_time(event_params) if event_params['start_date'].nil?
    event_params = define_end_time(event_params) if event_params['end_date'].nil?
    event_params
  end

  def self.define_duration(event_params)
    return event_params if event_params['start_date'].nil? | event_params['end_date'].nil?

    start_date = Date.parse(event_params['start_date'])
    end_date = Date.parse(event_params['end_date'])
    event_params['duration'] = (end_date - start_date).to_i
    event_params
  end

  def self.define_start_time(event_params)
    return event_params if event_params['duration'].nil? | event_params['end_date'].nil?

    end_date = Date.parse(event_params['end_date'])
    event_params['start_date'] = end_date - event_params['duration'].to_i.days
    event_params
  end

  def self.define_end_time(event_params)
    return event_params if event_params['start_date'].nil? | event_params['duration'].nil?

    start_date = Date.parse(event_params['start_date'])
    event_params['end_date'] = start_date + event_params['duration'].to_i.days
    event_params
  end

  private_class_method :define_duration, :define_start_time, :define_end_time
end
