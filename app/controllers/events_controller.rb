# frozen_string_literal: true
class EventsController < ApplicationController
  before_action :set_event, only: %i[show update destroy]
  # before_action :check_params_user_id, only: %i[create]

  # GET /events
  def index
    @events = Event.all
    json_response(@events)
  end

  # POST /events
  def create
    updated_params = update_event_params(event_params)
    @event = Event.create!(updated_params)
    json_response(@event, :created)
  end

  # GET /events/:id
  def show
    json_response(@event)
  end

  # PUT /events/:id
  def update
    @event.update(event_params)
    head :no_content
  end

  # DELETE /events/:id
  def destroy
    @event.status = 'Deleted'
    @event.save
    head :no_content
  end

  private

  def event_params
    # whitelist params
    params.permit(:duration, :start_date, :end_date, :name, :description, :location, :status, :user_id)
  end

  def update_event_params(event_params)
    updated_params = Event.define_status(event_params)
    Event.time_and_duration_setup(updated_params)
  end

  def set_event
    @event = Event.find(params[:id])
  end
end
