class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.integer :duration
      t.date :start_date
      t.date :end_date
      t.string :name
      t.text :description
      t.string :location
      t.string :status
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
